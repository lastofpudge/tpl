(function(start_point) {
	start_point(window.jQuery, window, document);

}(function($, window, document) {
	$(function() {
		'use strict';
		//default vars
		var _w = $(window);
		var _d = $(document);

		var mainOBJ = {
			wrapTABLE: function() {
				var table = $('.text-box').find('table');
				if (table.length) {
					table.wrap('<div class="table-wrapper"></div>');
				}
			},
			metrikaTargets: function(){
				_d.on('click', '.alaytics__target', function(event) {
					// yaCounterXXXXX.reachGoal('Goal_name');
				});
			}
		};

		//run
		mainOBJ.metrikaTargets(); //metrika target

		_w.load(function() {
			mainOBJ.wrapTABLE(); //wrap table to responsive
		});
	});
	// document ready
}));