'use strict';
var gulp     = require('gulp'),
sass         = require('gulp-sass'),
browserSync  = require('browser-sync').create(),
autoprefixer = require('gulp-autoprefixer'),
minifycss    = require('gulp-clean-css'),
csscomb      = require('gulp-csscomb'),
rename       = require('gulp-rename'),
jade         = require('gulp-jade'),
concat       = require('gulp-concat'),
uglify       = require('gulp-uglifyjs'),
plumber      = require('gulp-plumber'),
notify       = require('gulp-notify'),
shorthand    = require('gulp-shorthand');

/* BROWSER SYNC */
gulp.task('browser-sync', ['styles', 'scripts', 'jade'], function() {
	browserSync.init({
		server: {
			baseDir: "./app"
			},
			notify: false
			});

	});

/* CSS WORK */
gulp.task('styles', function () {
	return gulp.src('./_dev/sass/**/*.sass')
	.pipe(sass({
		includePaths: require('node-bourbon').includePaths
		}).on('error', sass.logError))
	.pipe(csscomb())
	.pipe(rename({suffix: '', prefix : ''}))
	.pipe(autoprefixer({browsers: ['last 4 versions'], cascade: false}))
	.pipe(shorthand())
	.pipe(minifycss())
	.pipe(gulp.dest('./app/css'))
	.pipe(browserSync.stream());
	});

/* JADE WORK */
gulp.task('jade', function() {
	return gulp.src('./_dev/jade/*.jade')
	.pipe(plumber())
	.pipe(jade())
	.pipe(gulp.dest('app'));
	});

/* JS WORK */
gulp.task('scripts', function() {
	return gulp.src([
		'./_dev/libs/modernizr/modernizr.js',
		'./_dev/libs/jquery/jquery-1.11.2.min.js',
		'./_dev/libs/common.js'
		])
		// .pipe(concat('libs.js')) //Concat js files
		// .pipe(uglify()) //Minify libs.js
		.pipe(gulp.dest('./app/js/'));
		});

/* COMPLETE */
gulp.task('watch', function () {
	gulp.watch('./_dev/sass/**/*.sass', ['styles']);
	gulp.watch('./_dev/jade/*.jade', ['jade']);
	gulp.watch('./_dev/libs/**/*.js', ['scripts']);
	gulp.watch('./app/js/*.js').on("change", browserSync.reload);
	gulp.watch('./app/*.html').on('change', browserSync.reload);
	});

/* DEFAULT */
gulp.task('default', ['browser-sync', 'watch']);
